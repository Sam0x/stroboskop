# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Sam0x@bitbucket.org/Sam0x/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Sam0x/stroboskop/commits/d4913c4df1ce8ece43e8e369656c21bbed7d49d6

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Sam0x/stroboskop/commits/4b1a1fc1c915e64f5c1acb3ac0f3d94c05dd25ff

Naloga 6.3.2:
https://bitbucket.org/Sam0x/stroboskop/commits/b7345bf3c61766e2d1a9f373e5d32b82c98436f5

Naloga 6.3.3:
https://bitbucket.org/Sam0x/stroboskop/commits/2c7d5691231fc772208402cf184350731694cc82

Naloga 6.3.4:
https://bitbucket.org/Sam0x/stroboskop/commits/fdd73e02a75227f9359acb6e71172e73420582b7

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Sam0x/stroboskop/commits/61f7fba3baf3c65e1823635ef82de1a4c2455bf8

Naloga 6.4.2:
https://bitbucket.org/Sam0x/stroboskop/commits/8de0faa077e1de7f9a9d7c7f7ae3f1e798e16253

Naloga 6.4.3:
https://bitbucket.org/Sam0x/stroboskop/commits/2da7b86aa5fb971eeea244540989d18635e593af

Naloga 6.4.4:
https://bitbucket.org/Sam0x/stroboskop/commits/fbaa5fd2c446fdc2e2211b9c20c994e06dbf7bd0